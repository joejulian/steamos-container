FROM quay.io/joejulian/arch:latest as bootstrap

ARG STEAMOS_RELEASE="brewmaster"
ARG STEAMOS_URL="http://repo.steampowered.com/steamos/"
ARG STEAMOS_BUILD_DIR="/bootstrap"

RUN pacman -Syu --force --noconfirm \
    bash \
    debootstrap \
    grep \
    gzip \
    sed \
    tar
    

COPY debootstrap.script /usr/share/debootstrap/scripts/${STEAMOS_RELEASE}

RUN mkdir -p ${STEAMOS_BUILD_DIR}/etc ; echo 'b46d9a8f7ecd45889403d181edbbc3c3' > $STEAMOS_BUILD_DIR/etc/machine-id

# Need to set this to lxc or else debootstrap tries to mknod
RUN container=lxc debootstrap \
        --components main,contrib,non-free \
        --merged-usr \
        --variant=minbase \
        --include=dbus-x11,steam-launcher,sudo,file,pciutils \
        --no-check-gpg \
        ${STEAMOS_RELEASE} ${STEAMOS_BUILD_DIR} ${STEAMOS_URL}

COPY steam.sudoer ${STEAMOS_BUILD_DIR}/etc/sudoers.d/steam

RUN rm -rf ${STEAMOS_BUILD_DIR}/bootstrap ${STEAMOS_BUILD_DIR}/var/lib/apt/lists/*

FROM scratch

ARG STEAMOS_BUILD_DIR="/bootstrap"

COPY --from=bootstrap ${STEAMOS_BUILD_DIR}/ /

RUN dpkg --add-architecture i386
RUN curl -Lo /tmp/ttf-wqy-microhei.deb 'http://launchpadlibrarian.net/102226290/ttf-wqy-microhei_0.2.0-beta-1ubuntu1_all.deb' && dpkg -i /tmp/ttf-wqy-microhei.deb
# RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y && apt-get install -y ttf-wqy-microhei && /usr/bin/rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y steamos-base-files libgl1-mesa-dri:i386 libgl1-mesa-glx:i386 libc6:i386 && apt-get -y upgrade && /usr/bin/rm -rf /var/lib/apt/lists/*

RUN adduser --disabled-password --gecos 'Steam' steam && adduser steam video

RUN mkdir -p /home/steam/.local/share/Steam && \
    tar xJ -C /home/steam/.local/share/Steam -f /usr/lib/steam/bootstraplinux_ubuntu12_32.tar.xz && \
    ln -s /home/steam/.local/share/Steam /home/steam/.steam/steam && \
    cp /usr/share/applications/steam.desktop /home/steam/Desktop && \
    chmod +x /home/steam/Desktop/steam.desktop && \
    cp /usr/lib/steam/bootstraplinux_ubuntu12_32.tar.xz /home/steam/.local/share/Steam/bootstrap.tar.xz && \
    chown -Rh steam:steam /home/steam

USER steam
ENV HOME=/home/steam
VOLUME /home/steam
CMD steam
