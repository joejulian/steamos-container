STEAMOS_RELEASE   := "brewmaster"
STEAMOS_URL       := "http://repo.steampowered.com/steamos/"
STEAMOS_BUILD_DIR := $(CURDIR)/bootstrap
STEAMOS_CONTAINER := steamos

CONTAINER_ROOT := $(CURDIR)/container/root
CONTAINER_RUN  := $(CURDIR)/container/run
CONTAINER_NAME := $(shell tr -dc 'a-f0-9' < /dev/urandom | fold -w 32 | head -n 1)

BUILDAH 		:= $(shell bash -c "command -v buildah")
BUILDAH_OPTIONS := --storage-driver vfs \
	--root $(CONTAINER_ROOT) \
	--runroot $(CONTAINER_ROOT)
#BUILDAH_OPTIONS := --storage-driver overlay

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

.PHONY: install_debootstrap_script
install_debootstrap_script: /usr/share/debootstrap/scripts/brewmaster ## Install the script necessary for debootstrap to install SteamOS
	@:

/usr/share/debootstrap/scripts/brewmaster: debootstrap_brewmaster.patch
	@cp /usr/share/debootstrap/scripts/sid $@
	@patch -p1 $@ < debootstrap_brewmaster.patch 2>&1 >/dev/null
	@echo brewmaster bootstrap script installed

.PHONY: bootstrap
bootstrap: install_debootstrap_script $(STEAMOS_BUILD_DIR)/ ## Create the SteamOS bootstrap directory

$(STEAMOS_BUILD_DIR)/:
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- apt-get update
	@sudo debootstrap \
		--merged-usr --variant=minbase \
		--include=dbus-x11,sudo \
		$(STEAMOS_RELEASE) $@ $(STEAMOS_URL)
	@cp -a steam.sudoer $@/etc/sudoers.d/steam

.PHONY: image
image: bootstrap #$(CONTAINER_ROOT)/ $(CONTAINER_RUN)/ ## Build the container image
	@$(BUILDAH) $(BUILDAH_OPTIONS) from --name $(CONTAINER_NAME) scratch
	BUILDAH="$(BUILDAH)" \
		BUILDAH_OPTIONS="$(BUILDAH_OPTIONS)" \
		CONTAINER_NAME="$(CONTAINER_NAME)" \
		STEAMOS_BUILD_DIR=$(STEAMOS_BUILD_DIR) \
		scripts/copy.sh
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- apt-get update
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- apt-get install -y steamos-base-files
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- adduser --disabled-password --gecos 'Steam' steam
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- adduser steam video
	@$(BUILDAH) $(BUILDAH_OPTIONS) config --user steam $(CONTAINER_NAME)
	@$(BUILDAH) $(BUILDAH_OPTIONS) config --env HOME=/home/steam $(CONTAINER_NAME)
	@$(BUILDAH) $(BUILDAH_OPTIONS) config --volume /home/steam $(CONTAINER_NAME)
	@$(BUILDAH) $(BUILDAH_OPTIONS) config --cmd steam $(CONTAINER_NAME)
	@$(BUILDAH) $(BUILDAH_OPTIONS) config --author "me@joejulian.name" --created-by "Joe Julian" --label name=steamos $(CONTAINER_NAME)
	@$(BUILDAH) $(BUILDAH_OPTIONS) run $(CONTAINER_NAME) -- rm -rf /var/lib/apt/lists/*
	@$(BUILDAH) $(BUILDAH_OPTIONS) commit $(CONTAINER_NAME) $(STEAMOS_CONTAINER)

# $(CONTAINER_ROOT)/:
# 	@mkdir -p $@

# $(CONTAINER_RUN)/:
# 	@mkdir -p $@
