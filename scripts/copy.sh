#!/bin/bash -x
DEST=$(${BUILDAH} ${BUILDAH_OPTIONS} mount ${CONTAINER_NAME})
cd ${STEAMOS_BUILD_DIR} && \
cp -av . ${DEST} && \
${BUILDAH} ${BUILDAH_OPTIONS} umount ${CONTAINER_NAME}
